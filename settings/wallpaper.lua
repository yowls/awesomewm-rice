local wallpaper = {}

local home = os.getenv("HOME")
local wallpaper_path = home .. "/library/Wallpaper/"

--=======================================================
-- METHOD
-----------------------
-- TODO: more options for gears.color
-- #> Set the method to work with
-- values:
-- 	'animated'	- Animated gif/mp4 [REQUIRE XWINWRAP]
-- 	'image'		- General images jpg/png
-- 	'color'		- Solid colors
wallpaper.method = "image"


--=======================================================
-- SLIDESHOW MODE
-----------------
-- NOTE: soon..


--=======================================================
-- RANDOM MODE
-----------------
-- NOTE: For image and animated, this will select a random wallpaper based on that directory
-- NOTE: For color, will choose a random color based on chosen color scheme
-- NOTE: This will overwrite static options
wallpaper.randomize = false

-- Select directory for randomize
-- will going recursive searching for files if there are another folders inside
wallpaper.image_dir = {
	wallpaper_path .. "Photography/",	-- monitor 1
	wallpaper_path				-- monitor 2
}
wallpaper.animated_dir = {
	wallpaper_path .. "nimated/",		-- monitor 1
	wallpaper_path .. "nimated/"		-- monitor 2
}


--=======================================================
-- STATIC MODE
-----------------
-- #> Select wallpaper by path
-- NOTE: only work if randomize = false
-- adds more paths for use on other monitors
-- if only are one path, all monitors will use the same wallpaper
-- value/s: path to image
wallpaper.image_wall = {
	wallpaper_path .. "Womart/90411135_p0.jpg",		-- all monitors
}
wallpaper.animated_wall = {
	wallpaper_path .. "/nimated/space.mp4",			-- all monitors
}
wallpaper.color_wall = {
	"#069420",		-- all monitors
}


return wallpaper
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap

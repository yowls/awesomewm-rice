----
-- Settings/dock_apps
-- set {apps,folder,links} to the dock
----

local defaults	= require('settings.default_apps')
local paths	= require('settings.paths')

local dock = {}


-- APPLICATIONS TO SHOW IN DOCK
----------------------------
-- choose your preferred applications to use

-- Generic icon names:
-- terminal, browser, text_editor, file_explorer, music_player, chat, folder

-- TODO: set options = {???}
dock.apps = {
	-- { name,	command,			icon }
	{ "Terminal",	defaults.terminal,		"kitty" },
	{ "Browser",	defaults.browser,		"firefox" },
	{ "Files",	defaults.explorer_alt,		"nemo" },
	{ "To-Do",	defaults.emacs .. paths.todo,	"emacs" },
}


-- FOLDERS TO SHOW IN DOCK
----------------------------
-- this will only show if extended mode is enabled

-- Generic folder paths:
-- home, desktop, downloads, documents, pictures, videos, trash

dock.folders = {
	-- { name,	folder path,		icon }
	{ "Downloads",	paths.downloads,	"folder" },
	{ "Documents",	paths.documents,	"folder" },
	{ "Drive",	"/backup/GDrive/",	"folder" }
}


-- TODO: add websites links section
-- TODO: add 4th section (special)

return dock
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap

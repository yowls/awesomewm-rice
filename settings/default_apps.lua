----
-- Settings/default_apps
-- set your applications of choice
----

local apps = {}


-- PREFERRED APPLICATIONS
----------------------------
-- choose your preferred applications to use
-- NOTE: this will use environment variables as the first option
--       if cannot find it, then go for second option

-- Terminals
apps.terminal		= os.getenv("TERMINAL") or "kitty"
apps.terminal_alt	= "urxvt"

-- Text Editors
apps.editor_cli		= os.getenv("EDITOR") or "nvim"
apps.editor_gui		= "vscodium"

-- File Managers
apps.explorer		= os.getenv("EXPLORER") or "nemo"
apps.explorer_alt	= "pcmanfm-qt"

-- Web Browsers
apps.browser		= os.getenv("BROWSER") or "firefox"
apps.browser_alt	= "chromium"

-- Others
apps.music_player	= "ncmpcpp"
apps.communication 	= "telegram-desktop"


--=======================================================

-- FIXING COMMAND FOR LAUNCH
apps.emacs		= "emacsclient -c -a 'emacs' "

-- use default terminal for run in-terminal apps
apps.editor_cmd	= apps.terminal .. " -e " .. apps.editor_cli


return apps
-- vim: tabstop=4 shiftwidth=4 softtabstop=0 noexpandtab nowrap

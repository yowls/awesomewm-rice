local gfs = require('gears.filesystem')
local paths = {}


-- DIRECTORIES
----------------

-- FIXME: use system xdg dirs
-- User dirs
paths.home		= os.getenv("HOME") .. "/"
paths.desktop		= paths.home .. "Desktop"
paths.downloads		= paths.home .. "Downloads"
paths.documents		= paths.home .. "Documents"
paths.pictures		= paths.home .. "Pictures"
paths.videos		= paths.home .. "Videos"

-- Awesome
paths.awesome		= gfs.get_configuration_dir()
paths.awesome_themes	= paths.awesome .. "themes/"
paths.sscripts		= paths.awesome .. "scripts/system/"

-- Cache
paths.cache_dir		= gfs.get_xdg_cache_home()
paths.awesome_cache	= paths.cache_dir .. "awesome/"


-- FILES
----------------
-- useful file locations

-- To-Do list with org mode
paths.todo		= "/backup/org/repos/awesome.org"

-- Awesome logs
-- at the moment, is just for the keybind to open it
paths.log_session	= paths.home .. ".xsession-errors"

paths.log_awesome_out	= paths.awesome_cache .. "stdout.log"
paths.log_awesome_err	= paths.awesome_cache .. "stderr.log"
paths.log_apps		= paths.awesome_cache .. "autostart_apps.log"


return paths
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap

local autostart = {}

local paths = require("settings.paths")
local configs_dir = paths.awesome .. "settings/configs"


--	SYSTEM
------------------------
-- Only will run applications that currently are not running
--
-- < left-side: name of program (for check if is running)
-- > right-side: command to run if is not running

autostart.system = {
	-- DESKTOP ENVIRONMENT INTEGRATION
	-- ["xfce-mcs-manager"] = "xfce-mcs-manager",

	-- POLICYKIT | PAM | KEYRING
	["polkit-gnome"] = "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1",
	-- ["gnome-keyring-daemon"] = "eval $(gnome-keyring-daemon -s --components=pkcs11,secrets,ssh,gpg)",

	-- X11 COMPOSITOR
	-- ["picom"] = "picom -b --backend glx --vsync",
	["picom"] = string.format("picom --config %s/picom_jonaburg.conf", configs_dir),

	-- SYSTEM LOCKSCREEN
	-- ["xidlehook"] = [[xidlehook --not-when-fullscreen --not-when-audio \
	--     --timer 300 'light -S 5' 'light -S 10' \
	--     --timer 590 'notify-send Suspending.. ' 'light -S 10' \
	--     --timer 600 'loginctl suspend' 'light -S 10']],

	-- DAEMONS
	["redshift"] = string.format("redshift -c %s/redshift.conf", configs_dir),
	["greenclip"] = "greenclip daemon"
}


--	APPLICATIONS
------------------------
-- Run applications only at the start of the window manager
-- and only if are not running
-- table parameters:
-- 		ps = process name, (or first table parameter)
-- 		cmd	= command to run, (omit if ps==cmd)
-- 		delay = delay in seconds (omit for no delay)

autostart.apps = {
	-- DAEMONS
	{ "mpd" },
	-- { "urxvt",		cmd = "urxvtd -q -o -f" },
	{ "emacs",		cmd = "emacs --daemon" },
	-- { "pcmanfm-qt",	cmd = "pcmanfm-qt --daemon-mode" },

	-- GRAPHICAL APPLICATIONS
	{ "kitty" },
	{ "telegram-desktop", delay = 3 },
	-- { "megasync", delay = 3 },
	-- { "transmission-qt", delay = 3 },
	{ "firefox", delay = 5 },
}


--	SCRIPTS
------------------------
-- Autostart custom scripts
-- these scripts will be executed every start/restart
-- wont check if is running and wont do any log

autostart.scripts = {
	"xrdb -merge ~/.Xresources",
}


return autostart

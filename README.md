<!-- BANNER -->
<div align='center'>
<img src="assets/banner.png" align=center height=256px>
</div>

<!-- SUBTITLE -->
<div align='center'>
<img src="https://img.shields.io/maintenance/yes/2021?color=3a86ff&style=flat-square" height=25px>
<img src="https://img.shields.io/badge/Git_version-3a86ff?style=for-the-badge&logo=git&logoColor=white" height=25px>
</div>



<br>



<!-- DESCRIPTION -->
<div align='left'>
<img src="assets/description.png" align=center height=125px>
</div>
<br>

**[Awesome](https://awesomewm.org/)** is a dynamic [window manager](https://wiki.archlinux.org/title/Window_manager) for X11.<br>
Is the most extensive, fully featured and complete there is, in my opinion.<br>
You can customize it to fit precisely your needs, if you know how to program.<br>
It's also written from scratch in C and configured with lua.<br>
[read more..](https://gitlab.com/yowls/awwm/-/wikis/home)

<br>

🪶 This repository only contains my config for *Awesome*,<br>
which is running on the `4.3-git` version

<br>

###### 🚀 *QUICK INSTALL*

```sh
LINK="https://gitlab.com/yowls/awwm/-/raw/main/scripts/misc/setup.sh" && [ $(command -v curl) ] && sh -c "$(curl -fsSL $LINK)" -O -yd || [ $(command -v wget) ] && sh -c "$(wget -O- $LINK)" -O -yd || [ $(command -v fetch) ] && sh -c "$(fetch -o - $LINK)" -O -yd || echo 'install curl, wget or fetch first'
```

*|=> [ [source script](scripts/misc/setup.sh) ]*



<br>
<br>


<!-- PREVIEWS -->
<div align='left'>
<img src="assets/previews.png" align=center height=125px>
</div>
<br>

<div align='center'>
	<img src="https://i.imgur.com/C4aD5PI.png" align=center height=450px>
</div>



<br>
<br>



<!-- FEATURES -->
<div align='left'>
<img src="assets/features.png" align=center height=125px>
</div>
<br>

+ Easily configurable based on user settings
+ Rules for floating windows and window matching
+ Easily extensible with Lua



<br>
<br>



<!-- DOCUMENTATION -->
<div align='left'>
<img src="assets/documentation.png" align=center height=125px>
</div>
<br>

link to wiki pages

{ *[Home Wiki](https://gitlab.com/yowls/awwm/-/wikis/home)* } ||
{ *[Installation](https://gitlab.com/yowls/awwm/-/wikis/Install)* } ||
{ *[Explanation of the code](https://gitlab.com/yowls/awwm/-/wikis/explanation-of-code)* }

----
-- COLOR SCHEME
--  ayu colors
----

-- Return dark and light modes
return {
	light	= require(... .. '.light'),
	dark	= require(... .. '.dark')
}

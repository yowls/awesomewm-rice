-- AYU COLOR SCHEME
-- dark mode

return {
	-- Background
	bg	= "#0A0E14",
	bgi	= "#0A0E14" .. "88",	-- inactive

	-- Foreground
	fg	= "#B3B1AD",
	fgi	= "#626A73",	-- inactive

	-- Special
	accent	= "#E6B450",
	alert	= "#FF3333",

	-- Palette
	color1	= "#F07178",	-- red
	color2	= "#C2D94C",	-- green
	color3	= "#FFEE99",	-- yellow
	color4	= "#59C2FF",	-- blue
	color5	= "#E6B673",	-- magenta
	color6	= "#FF8F40" 	-- cyan
}

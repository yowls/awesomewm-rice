-- MINLA
-- light mode

return {
	-- Background
	bg	= "#F5F6FA",
	bgi	= "#A2A2A4" .. "88",	-- inactive

	-- Foreground
	fg	= "#333641",
	fgi	= "#333641",	-- inactive

	-- Special
	accent	= "#778BEB",
	alert	= "#FF4D4D",

	-- Palette
	color1	= "#FF4D4D",	-- red
	color2	= "#38ADA9",	-- green
	color3	= "#F7D794",	-- yellow
	color4	= "#546DE5",	-- blue
	color5	= "#786FA6",	-- magenta
	color6	= "#778BEB" 	-- cyan
}

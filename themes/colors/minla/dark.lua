-- MINLA
-- dark mode

return {
	-- Background
	bg	= "#333641",
	bgi	= "#333641" .. "88",	-- inactive

	-- Foreground
	fg	= "#F5F6FA",
	fgi	= "#A2A2A4",	-- inactive

	-- Special
	accent	= "#778BEB",
	alert	= "#FF4D4D",

	-- Palette
	color1	= "#FF4D4D",	-- red
	color2	= "#38ADA9",	-- green
	color3	= "#F7D794",	-- yellow
	color4	= "#546DE5",	-- blue
	color5	= "#786FA6",	-- magenta
	color6	= "#778BEB" 	-- cyan
}

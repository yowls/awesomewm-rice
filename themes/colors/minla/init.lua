----
-- COLOR SCHEME
----

-- Return dark and light modes
return {
	light	= require(... .. '.light'),
	dark	= require(... .. '.dark')
}

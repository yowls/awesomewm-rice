---
-- SET THE GLOBAL COLOR SCHEME
---

local settings = require('settings.theming')

return function ()
	-- Load a random color scheme if the option is set to true
	local palette
	if settings.random_color_scheme then
		palette = settings.color_scheme_themes [ math.random(#settings.color_scheme_themes) ]
	else
		palette = settings.color_scheme
	end

	-- Load the colors in dark or light mode
	if settings.color_scheme_mode == "light" then
		return require("themes.colors." .. palette .. ".light")

	elseif settings.color_scheme_mode == "dark" then
		return require("themes.colors." .. palette .. ".dark")
	end
end

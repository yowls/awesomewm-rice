---
-- SET GENERAL THEME SETTINGS
---

local dpi = require("beautiful.xresources").apply_dpi
local recolor = require("gears.color").recolor_image
local settings = require('settings.theming')

local append_theme = function ( theme )
	-- shortcuts
	local cs = theme.cs
	local icons = theme.icons

	return {
		-- GENERAL
		icon_theme	= settings.global_icon,
		font		= settings.global_font,

		bg_normal	= cs.bg,
		bg_focus	= cs.accent,
		bg_urgent	= cs.alert,
		bg_minimize	= cs.bg,
		bg_systray	= cs.bg,

		fg_normal	= cs.fg,
		fg_focus	= cs.bg,
		fg_urgent	= cs.bg,
		fg_minimize	= cs.fgi,

		border_normal	= cs.fgi,
		border_focus	= cs.accent,
		border_marked	= cs.color1,

		useless_gap	= dpi(settings.window_margin),
		border_width	= dpi(settings.window_border),

		awesome_icon	= recolor(icons.awesomewm, cs.accent),

		-- TOOLTIP
		tooltip_border_color	= cs.accent,
		tooltip_border_width	= dpi(settings.window_border),
		tooltip_bg		= cs.bg,
		tooltip_fg		= cs.fg,
		tooltip_font		= settings.global_font,
		-- tooltip_opacity	= "",
		-- tooltip_align	= "",
		-- tooltip_shape	= function(cr, w, h)
		--     gears.shape.rounded_rect(cr, w, h, dpi(6))
		-- end,

		-- DESKTOP MENU
		-- menu_submenu_icon	= recolor(icons.awesomewm, cs.accent),
		menu_submenu		= "|>",
		menu_font		= settings.global_font,
		-- menu_height		= dpi(50),
		-- menu_width		= dpi(150),
		menu_border_color	= cs.accent,
		menu_border_width	= dpi(settings.window_border),
		menu_fg_normal		= cs.fg,
		menu_fg_focus		= cs.bg,
		menu_bg_normal		= cs.bg,
		menu_bg_focus		= cs.color3,

		-- HOTKEYS POPUP
		hotkeys_bg		= cs.bg,
		hotkeys_fg		= cs.fg,
		hotkeys_label_bg	= cs.color4,
		hotkeys_label_fg	= cs.bg,
		hotkeys_modifiers_fg	= cs.color4,
		hotkeys_group_margin	= dpi(10),
		hotkeys_border_width	= dpi(settings.window_border),
		hotkeys_border_color	= cs.accent,
		hotkeys_font		= settings.global_font,
		hotkeys_description_font = settings.notification_font,
		-- hotkeys_shape	= ""

		-- PROMPT
		prompt_fg		= cs.fg,
		prompt_bg		= cs.bg,
		prompt_fg_cursor	= cs.accent,
		prompt_bg_cursor	= cs.bg,
		prompt_font		= settings.global_font,
	}
end

return append_theme
-- vim: tabstop=8:shiftwidth=8:softtabstop=0:noexpandtab:nowrap

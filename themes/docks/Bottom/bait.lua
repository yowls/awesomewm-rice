---
-- Extra bar to pull action
---

local wibox = require('wibox')


-- FIXME: add connect_signal mouse enter for show others dock
-- FIXME: set width of the dock bar
-- TODO: hide if the dock is being shown
local minibar = function(s)
	-- Create a empty bar
	local bar = wibox {
		screen	= s,
		ontop	= true,
		visible	= true,
		height	= 5,
		width	= s.geometry.width,
		bg		= "#ff00ff"
		-- bg		= "#00000000"
	}

	-- Add mouse actions
	bar:connect_signal('mouse::enter', function()
		awesome.emit_signal("dock::show")
	end)

	return bar
end

return minibar

---
-- Create a list of application widgets
-- based on user settings (settings/dock_apps)
---

local wibox = require('wibox')

-- IMPORT THE TEMPLATE FOR CREATE WIDGETS
local base	= require('themes.docks.Bottom.widgets.apps.base')
local fav	= require('settings.dock_apps').apps


-- MAIN
local widget = function()
	local apps = {
		layout = wibox.layout.fixed.horizontal
	}

	for _,name in pairs(fav) do
		local one_widget = base(name[1], name[2], name[3])
		table.insert(apps, one_widget)
	end

	-- return a table with widgets
	return wibox.widget {
		apps,
		layout = wibox.layout.align.horizontal
	}
end

return widget

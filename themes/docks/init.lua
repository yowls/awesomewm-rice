---
-- LOAD DOCK THEME
---

local settings = require('settings.theming')

if settings.enable_dock then
	local theme

	if settings.random_dock then
		theme = settings.dock_themes[ math.random(#settings.dock_themes) ]
	else
		theme = settings.dock
	end

	require('themes.docks.' .. theme)
end

local icons = {}
-- Bind all paths of 'Min' icons here

local paths	= require("settings.paths")
local dir	= paths.awesome_themes .. "icons/Min/"
local err_dir	= dir .. "errors/"


-- Set awesome icon
icons.awesomewm		= dir .. "awesomewm.svg"


-- Set Titlebar icons path
-- TODO: focus mode?
local titlebar_dir = dir .. "titlebar/"
icons.titlebar = {
	close = {
		normal	= titlebar_dir .. "normal_button.svg",
		hover	= titlebar_dir .. "close_hover.svg"
	},
	floating = {
		normal	= titlebar_dir .. "normal_button.svg",
		hover	= titlebar_dir .. "floating_hover.svg"
	},
	maximized = {
		normal	= titlebar_dir .. "normal_button.svg",
		hover	= titlebar_dir .. "maximized_hover.svg"
	},
	minimize = {
		normal	= titlebar_dir .. "normal_button.svg",
		hover	= titlebar_dir .. "minimize_hover.svg"
	},
	sticky = {
		normal	= titlebar_dir .. "normal_button.svg",
		hover	= titlebar_dir .. "sticky_hover.svg"
	},
	top = {
		normal	= titlebar_dir .. "normal_button.svg",
		hover	= titlebar_dir .. "top_hover.svg"
	}
}


-- Set Layout icons
local layout_dir = dir .. "layout/"
icons.layout = {
	tile		= layout_dir .. "tile.svg",
	tile_top	= layout_dir .. "tile_top.svg",
	tile_left	= layout_dir .. "tile_left.svg",
	tile_bottom	= layout_dir .. "tile_bottom.svg",
	spiral		= layout_dir .. "spiral.svg",
	spiral_dwindle	= layout_dir .. "spiral.svg",
	max		= layout_dir .. "max.svg",
	magnifier	= layout_dir .. "magnifier.svg",
	fullscreen	= layout_dir .. "fullscreen.svg",
	floating	= layout_dir .. "floating.svg",
	fair_vertical	= layout_dir .. "fair_vertical.svg",
	fair_horizontal	= layout_dir .. "fair_horizontal.svg",
	corner_sw	= layout_dir .. "corner_sw.svg",
	corner_se	= layout_dir .. "corner_se.svg",
	corner_nw	= layout_dir .. "corner_nw.svg",
	corner_ne	= layout_dir .. "corner_ne.svg"
}


-- Set Taglist icons
local taglist_dir = dir .. "taglist/"
icons.taglist = {
	t1	= taglist_dir .. "empty.svg",
	t2	= taglist_dir .. "empty.svg",
	t3	= taglist_dir .. "empty.svg",
	t4	= taglist_dir .. "empty.svg",
	t5	= taglist_dir .. "empty.svg",
	t6	= taglist_dir .. "empty.svg",
	t7	= taglist_dir .. "empty.svg",
	t8	= taglist_dir .. "empty.svg",
	t9	= taglist_dir .. "empty.svg",
	t0	= taglist_dir .. "empty.svg",
	selected	= taglist_dir .. "busy.svg",
	unselected	= taglist_dir .. "empty.svg",
	urgent		= taglist_dir .. "urgent.svg",
	focus		= taglist_dir .. "focus.svg",
	unfocus		= taglist_dir .. "empty.svg"
}


-- Set System indicator icons
local system_dir = dir .. "system/"
icons.system = {
	-- Brightness
	brightness	= system_dir .. "brightness.svg",
	brightness_1	= system_dir .. "brightness.svg",
	brightness_2	= system_dir .. "brightness.svg",
	brightness_3	= system_dir .. "brightness.svg",

	-- Volume
	volume		= system_dir .. "volume_3.svg",
	volume_1	= system_dir .. "volume_1.svg",
	volume_2	= system_dir .. "volume_2.svg",
	volume_3	= system_dir .. "volume_3.svg",
	volume_muted	= system_dir .. "volume_muted.svg",

	-- Battery
	battery		= system_dir .. "battery_5.svg",
	battery_1	= system_dir .. "battery_1.svg",
	battery_2	= system_dir .. "battery_2.svg",
	battery_3	= system_dir .. "battery_3.svg",
	battery_4	= system_dir .. "battery_4.svg",
	battery_5	= system_dir .. "battery_5.svg",

	-- Network
	network		= system_dir .. "network_3.svg",
	network_1	= system_dir .. "network_1.svg",
	network_2	= system_dir .. "network_2.svg",
	network_3	= system_dir .. "network_3.svg",
	network_error	= system_dir .. "network_error.svg",

	-- Misc
	clock		= system_dir .. "clock.svg",
	date		= system_dir .. "date.svg",
	cpu		= system_dir .. "cpu.svg",
	memory		= system_dir .. "memory.svg",
	screenshot	= system_dir .. "screenshot.svg",

	-- Errors
	unable		= err_dir .. "unable.svg",
	unknown		= err_dir .. "unknown.svg"
}


-- Set some Applications icons
local applications_dir = dir .. "applications/"
icons.applications = {
	-- Generic
	terminal	= applications_dir .. "terminal.svg",
	text_editor	= applications_dir .. "text_editor.svg",
	file_explorer	= applications_dir .. "file_explorer.svg",
	browser		= applications_dir .. "browser.svg",
	music_player	= applications_dir .. "music_player.svg",
	chat		= applications_dir .. "chat.svg",

	-- Specific
	redshift	= applications_dir .. "redshift.svg",

	-- Errors
	unknown		= err_dir .. "unknown.svg",
	alert		= err_dir .. "alert.svg"
}


return icons
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap

local wibox = require('wibox')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi


-- MAIN
local widget = function()
	return wibox.widget {
		orientation		= 'horizontal',
		forced_width	= dpi(5),
		thickness		= 0,
		widget			= wibox.widget.separator
	}
end

return widget

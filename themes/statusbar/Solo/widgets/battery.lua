local awful = require('awful')
local wibox = require('wibox')
local gears = require('gears')
local beautiful = require('beautiful')
local icons = beautiful.icons.system
local watch = awful.widget.watch
local recolor = require("gears.color").recolor_image

-- NOTE: require acpi

-- TODO: add mouse buttons
-- TODO: add tooltip
-- TODO: add popup


-- MAIN
local widget = {}
local function battery(args)
	-- Settings
	local settings	= args or {}
	local font		= settings.font		or beautiful.statusbar_font
	local refresh	= settings.refresh	or 20

	-- =======================================

	-- Create the widget
	local widget_icon = wibox.widget {
		{
			id = "icon",
			widget = wibox.widget.imagebox,
			image = recolor(icons.battery, beautiful.cs.fg),
			resize = true
		},
		valigh = 'center',
		layout = wibox.container.place
	}

	local widget_text = wibox.widget {
		widget = wibox.widget.textbox,
		font = font
	}

	widget = wibox.widget {
		widget_icon,
		widget_text,
		layout = wibox.layout.fixed.horizontal
	}

	-- =======================================

	local function get_status (w, stdout)
		-- clean output
		-- output: Battery 0: Full, 100%
		local battery_info = {}
		local capacities = {}
		for s in stdout:gmatch("[^\r\n]+") do
			local status, charge_str, remaining = string.match(s, '.+: (%a+), (%d?%d?%d)%%,?(.*)')
			if status ~= nil then
				table.insert(battery_info, {status = status, charge = tonumber(charge_str)})
			else
				local cap_str = string.match(s, '.+:.+last full capacity (%d+)')
				table.insert(capacities, tonumber(cap_str))
			end
		end

		local capacity = 0
		for _, cap in ipairs(capacities) do
			capacity = capacity + cap
		end

		local charge = 0
		local status
		for i, batt in ipairs(battery_info) do
			if capacities[i] ~= nil then
				if batt.charge >= charge then
					status = batt.status
				end
				charge = charge + batt.charge * capacities[i]
			end
		end
		charge = charge / capacity

		-- set widget text of charge
		widget_text.text = string.format('%d%%', charge)

		-- set widget icon
		local icon_state
		if (charge < 20) then icon_state = icons.battery_1
		elseif (charge >= 20 and charge < 40) then icon_state = icons.battery_2
		elseif (charge >= 40 and charge < 60) then icon_state = icons.battery_3
		elseif (charge >= 60 and charge < 80) then icon_state = icons.battery_4
		elseif (charge >= 80) then icon_state = icons.battery_5
		end

		-- if status == 'Charging' then
		--     icon_state = ""
		-- else
		--     icon_state = ""
		-- end

		w.icon:set_image(gears.surface.load_uncached( recolor(icon_state, beautiful.cs.fg) ))
	end

	-- update widget
	watch("acpi -i", refresh, get_status, widget_icon)

	return widget
end


-- Return widget
return setmetatable(widget, {
	__call = function(_, ...)
		return battery(...)
	end
})

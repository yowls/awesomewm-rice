local awful = require('awful')

local layoutbox = {}

-- TODO: add popup
-- TODO: add tooltip


-- BUTTONS FUNCTIONS
layoutbox.buttons = {
	awful.button({ }, 1, function () awful.layout.inc( 1) end),
	awful.button({ }, 3, function () awful.layout.inc(-1) end),
	awful.button({ }, 4, function () awful.layout.inc(-1) end),
	awful.button({ }, 5, function () awful.layout.inc( 1) end),
}


-- MAIN
local widget = function(s)
	return awful.widget.layoutbox {
		screen  = s,
		buttons = layoutbox.buttons
	}
end

return widget

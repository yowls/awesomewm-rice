local awful = require('awful')
local wibox = require('wibox')
local gears = require('gears')
local beautiful = require('beautiful')
local icons = beautiful.icons.system
local recolor = require("gears.color").recolor_image

-- TODO: add mouse buttons
-- TODO: add tooltip
-- TODO: add popup
-- TODO: update icon based on volume %
-- TODO: add mute indicator


-- MAIN
local widget = {}
local function Volume(args)
	-- Settings
	local settings	= args or {}
	local font		= settings.font		or beautiful.statusbar_font

	-- =======================================

	-- Create the widget
	local widget_icon = wibox.widget {
		{
			id = "icon",
			widget = wibox.widget.imagebox,
			image = recolor(icons.volume, beautiful.cs.fg),
			resize = true
		},
		valigh = 'center',
		layout = wibox.container.place
	}

	local widget_text = wibox.widget {
		widget = wibox.widget.textbox,
		font = font
	}

	widget = wibox.widget {
		widget_icon,
		widget_text,
		layout = wibox.layout.fixed.horizontal
	}

	-- =======================================

	-- TODO: use self
	function widget:update ()
		awful.spawn.easy_async_with_shell("sh -c 'amixer -D pulse sget Master'", function(stdout)
			-- clean output
			-- output: ..

			local value = tonumber( string.match(stdout, '(%d?%d?%d)%%') )

			-- set widget text
			widget_text.text = tostring(value) .. "%"

			-- set widget icon
			local icon_state
			if value < 35 then icon_state = icons.volume_1
			elseif value < 70 then icon_state = icons.volume_2
			else icon_state = icons.volume_3
			end

			widget_icon.icon:set_image(gears.surface.load_uncached( recolor(icon_state, beautiful.cs.fg) ))
		end)
	end

	-- function widget:toggle_mute()

	-- =======================================

	-- Signals
	-- widget:connect_signal('button::press', function()
	-- widget:connect_signal('mouse::enter', function()
	-- widget:connect_signal('mouse::leave', function()

	-- =======================================

	-- Start widget
	widget:update()

	return widget
end


-- Updates receiving a signal
awesome.connect_signal("system::volume:update", function()
	widget:update()
end)

awesome.connect_signal("system::volume:set", function(value)
	widget.text = value
end)



-- Return widget
return setmetatable(widget, {
	__call = function(_, ...)
		return Volume(...)
	end
})

local awful = require('awful')
local beautiful = require('beautiful')
local menu = require('library.menu')

-- DESCRIPTION
-- Create a Awesome desktop menu

-- TODO: add tooltip


-- MAIN
local widget = function()
	return awful.widget.launcher {
		image	= beautiful.awesome_icon,
		menu	= menu.main
	}
end

return widget

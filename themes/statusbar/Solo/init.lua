-- IMPORT BARS
local path = "themes.statusbar.Solo."
local settings = require("settings.theming")


-- SET THE STATUS BAR FOR EACH SCREEN
screen.connect_signal("request::desktop_decoration", function(s)
	if s.index == 1 then
		-- Main screen
		s.main_bar = require(path .. "simple")(s)

		if settings.statusbar_mode == "extended" then
			s.compost_bar = require(path .. "compost")(s)
		end

	else
		-- Any other screen
		s.external = require(path .. "multi")(s)

	end
end)

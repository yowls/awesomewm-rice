local awful = require('awful')
local wibox = require('wibox')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi

-- IMPORT WIDGETS
local widgets_path = "themes.statusbar.Solo.widgets."
local taglist_f		= require(widgets_path .. "taglist")
local layoutbox_f	= require(widgets_path .. "layoutbox")
local menu_f		= require(widgets_path .. "menu")
local clock_f		= require(widgets_path .. "clock")
local date_f		= require(widgets_path .. "date")
local battery_f		= require(widgets_path .. "battery")
local volume_f		= require(widgets_path .. "volume")
local ram_f		= require(widgets_path .. "ram")
local sep_f		= require(widgets_path .. "separator")
local prompt_f		= awful.widget.prompt
local systray_f		= wibox.widget.systray


-- ADD WIDGET BACKGROUND
-- local function wrap_widget ()
-- end


-- CREATE TABLE OF WIDGETS
local init_widgets = function(s)

	-- Build widgets functions
	local menu	= menu_f()
	local clock	= clock_f()
	local date	= date_f()
	local battery	= battery_f()
	local volume	= volume_f()
	local ram	= ram_f()
	local sep	= sep_f()

	-- Set widgets only in this screen
	s.taglist	= taglist_f(s)
	s.layoutbox	= layoutbox_f(s)
	s.systray	= systray_f()
	s.prompt	= prompt_f()

	-----------------------------

	-- Set position of widgets
	local widgets_on = {}

	widgets_on.left = {
		-- widgets
		sep,
		menu,
		s.taglist,
		s.prompt,
		-- properties
		spacing	= 5,
		layout	= wibox.layout.fixed.horizontal
	}

	-- NOTE: only one widget
	widgets_on.middle = date

	widgets_on.right = {
		-- widgets
		s.systray,
		ram,
		volume,
		battery,
		clock,
		s.layoutbox,
		-- properties
		spacing	= 5,
		layout	= wibox.layout.fixed.horizontal
	}

	return widgets_on
end


-- SET MAIN STATUS BAR
local simple = function(s)

	-- Settings
	local height	= 30	-- in pixels

	-- Create the wibox bar
	local bar = wibox {
		screen		= s,
		visible		= true,
		ontop		= true,
		type		= 'dock',
		height		= dpi(height),
		width		= s.geometry.width,
		x		= s.geometry.x,
		y		= s.geometry.y,
		bg		= beautiful.bg_normal,
		fg		= beautiful.fg_normal,
		-- border_color	= beautiful.bg_normal,
		-- border_width	= beautiful.bg_normal
	}

	-- Reserve space in the screen
	bar:struts { top = dpi(height) }

	-- Add widgets
	local bar_widgets = init_widgets(s)

	bar:setup {
		-- widgets
		bar_widgets.left,
		bar_widgets.middle,
		bar_widgets.right,
		-- properties
		expand = "none",
		layout = wibox.layout.align.horizontal
	}

	return bar
end

return simple
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap

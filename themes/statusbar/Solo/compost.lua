local wibox = require('wibox')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi

-- IMPORT WIDGETS
local widgets_path = "themes.statusbar.Solo.widgets."
local tasklist_f	= require(widgets_path .. "tasklist")


-- ADD WIDGET BACKGROUND
-- local function wrap_widget ()
-- end


-- CREATE TABLE OF WIDGETS
local init_widgets = function(s)

	-- Build widgets functions
	-- ..

	-- Set widgets only in this screen
	s.tasklist	= tasklist_f(s)

	-----------------------------

	-- Set position of widgets
	local widgets_on = {}

	widgets_on.left = nil

	-- NOTE: only one widget
	widgets_on.middle = s.tasklist

	widgets_on.right = nil

	return widgets_on
end


-- SET STATUS BAR AS A COMPLEMENT OF MAIN
local compost = function(s)

	-- Settings
	local height	= 25	-- in pixels

	-- Create the bar
	local bar = wibox {
		screen		= s,
		visible		= true,
		ontop		= true,
		type		= 'dock',
		height		= dpi(height),
		width		= s.geometry.width,
		x		= s.geometry.x,
		y		= dpi(s.geometry.height - height),
		bg		= beautiful.bg_normal,
		fg		= beautiful.fg_normal,
		-- border_color	= beautiful.bg_normal,
		-- border_width	= beautiful.bg_normal
	}

	-- Reserve space in the screen
	bar:struts { bottom = dpi(height) }

	-- Add widgets
	local bar_widgets = init_widgets(s)

	bar:setup {
		-- widgets
		bar_widgets.left,
		bar_widgets.middle,
		bar_widgets.right,
		-- properties
		expand = "none",
		layout = wibox.layout.align.horizontal
	}

	return bar
end

return compost
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap

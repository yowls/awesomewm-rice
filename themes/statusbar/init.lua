---
-- LOAD STATUS BAR THEME
---

local settings = require('settings.theming')

if settings.enable_statusbar then
	local theme

	if settings.random_statusbar then
		theme = settings.statusbar_themes[ math.random(#settings.statusbar_themes) ]
	else
		theme = settings.statusbar
	end

	require('themes.statusbar.' .. theme)
end

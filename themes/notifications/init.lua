---
-- LOAD NOTIFICATION THEME
---

local settings = require('settings.theming')
local theme

if settings.random_notification then
	theme = settings.notification_themes[ math.random(#settings.notification_themes) ]
else
	theme = settings.notification
end

require('themes.notifications.' .. theme)

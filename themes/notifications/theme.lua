---
-- SET THEME SETTINGS FOR NAUGHTY NOTIFICATION
---

local dpi = require("beautiful.xresources").apply_dpi
local settings = require('settings.theming')

local append_theme = function ( theme )
	return {
		notification_font	= settings.notification_font,
		notification_bg		= theme.cs.bg,
		notification_fg		= theme.cs.fg,

		-- TODO: merge this in notification theme
		notification_border_color	= theme.cs.fg,
		notification_icon_size		= dpi(64),
		notification_opacity		= 0.95,
		notification_width			= dpi(200),
		notification_height			= dpi(40),
		notification_max_width		= dpi(300),
		notification_max_height		= dpi(80)
	}
end

return append_theme

local awful = require("awful")
local wibox = require("wibox")
local beautiful = require('beautiful')

-- OPTIONS
awful.titlebar.enable_tooltip = true
awful.titlebar.fallback_name  = 'Client'


-- BUTTONS
local buttons = function(c)
	return {
		awful.button({ }, 1,
			function()
				c:activate { context = "titlebar", action = "mouse_move"  }
			end
		),
		-- awful.button({  }, 2, function() end),
		awful.button({ }, 3,
			function()
				c:activate { context = "titlebar", action = "mouse_resize"}
			end
		)
	}
end


-- CREATE TABLE OF WIDGETS
local init_widgets = function(c)

	-- Build titlebar widgets functions
	local ontop		= awful.titlebar.widget.ontopbutton(c)
	local sticky	= awful.titlebar.widget.stickybutton(c)
	local title		= awful.titlebar.widget.titlewidget(c)
	local floating	= awful.titlebar.widget.floatingbutton(c)
	local maximized	= awful.titlebar.widget.maximizedbutton(c)
	-- local minimize	= awful.titlebar.widget.minimizebutton(c)
	local close		= awful.titlebar.widget.closebutton(c)

	-- Set position of widgets
	local widgets_on = {}

	widgets_on.left = {
		-- widgets
		ontop,
		sticky,
		-- properties
		layout	= wibox.layout.fixed.horizontal
	}

	widgets_on.middle = {
		-- widgets
		widget	= title,
		-- properties
		align	= "center"
	}

	widgets_on.right = {
		-- widgets
		floating,
		maximized,
		close,
		-- properties
		layout = wibox.layout.fixed.horizontal()
	}

	return widgets_on
end


-- SET THE TITLEBAR
client.connect_signal("request::titlebars", function(c)

	-- Create the title bar
	local titlebar = awful.titlebar(c, {
		size		= 30,
		position	= "top",
		font		= beautiful.titlebar_font
	})

	-- Set widgets to the titlebar
	local titlebar_widgets = init_widgets(c)

	titlebar : setup {
		-- widgets
		titlebar_widgets.left,
		{ -- Middle side
			titlebar_widgets.middle,
			buttons = buttons(c),
			layout  = wibox.layout.flex.horizontal
		},
		titlebar_widgets.right,
		-- properties
		layout = wibox.layout.align.horizontal
	}
end)

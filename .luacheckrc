-- Custom sets of globals

read_globals = {
	-- Awesome API
	"awesome",
	"button",
	"dbus",
	"drawable",
	"drawin",
	"key",
	"keygrabber",
	"mousegrabber",
	"root",
	"selection",
	"tag",
	"window",

	-- Custom
	-- keybinds
	"M",
	"A",
	"S",
	"C"
}

globals = {
	-- awesome API
	"screen",
	"mouse",
	"client"
}

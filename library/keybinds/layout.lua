local awful = require('awful')

local group_name = "layout"


-- LAYOUTS
awful.keyboard.append_global_keybindings {
	awful.key({M},"space",
		function () awful.layout.inc( 1) end,
		{group = group_name, description = "select next"}
	),
	awful.key({M, S}, "space",
		function () awful.layout.inc(-1) end,
		{group = group_name, description = "select previous"}
	),
	awful.key({M, S}, "h",
		function () awful.tag.incnmaster( 1, nil, true) end,
		{group = group_name, description = "increase the number of master clients"}
	),
	awful.key({M, S}, "l",
		function () awful.tag.incnmaster(-1, nil, true) end,
		{group = group_name, description = "decrease the number of master clients"}
	),
	awful.key({M, C}, "h",
		function () awful.tag.incncol( 1, nil, true) end,
		{group = group_name, description = "increase the number of columns"}
	),
	awful.key({M, C}, "l",
		function () awful.tag.incncol(-1, nil, true) end,
		{group = group_name, description = "decrease the number of columns"}
	)
}
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap

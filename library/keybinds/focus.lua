local awful = require('awful')
local helpers	= require("library.helpers.keybinds")

local group_name = "Focus clients"


-- FOCUS CLIENTS
awful.keyboard.append_global_keybindings {
	awful.key({M}, "j",
		function () awful.client.focus.byidx( 1)end,
		{group = group_name, description = "focus next by index"}
	),
	awful.key({M}, "k",
		function () awful.client.focus.byidx(-1) end,
		{group = group_name, description = "focus previous by index"}
	),
	awful.key({M}, "u",
		awful.client.urgent.jumpto,
		{group = group_name, description = "jump to urgent client"}
	),
	awful.key({A}, "Tab",
		helpers.focus_previous,
		{group = group_name, description = "go back?"}
	),
	awful.key({M, S}, "n",
		helpers.minimize,
		{group = group_name, description = "restore minimized"}
	)
}
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap

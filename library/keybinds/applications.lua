local awful = require('awful')

local apps	= require("settings.default_apps")
local scripts_path = require("settings.paths").sscripts

local group_name = {
	default		= "Default apps",
	rest		= "Applications",
	screenshot	= "Screenshot"
}


-- USER APPLICATIONS
awful.keyboard.append_global_keybindings {
	-- Prefered applications
	awful.key({M}, "b",
		function() awful.spawn(apps.browser) end,
		{group = group_name.default, description= "Launch Browser"}
	),
	awful.key({M}, "e",
		function() awful.spawn(apps.explorer) end,
		{group = group_name.default, description= "Launch File manager"}
	),
	awful.key({M}, "v",
		function() awful.spawn(apps.editor_gui) end,
		{group = group_name.default, description= "Launch graphical text editor"}
	),
	awful.key({M, S}, "b",
		function() awful.spawn(apps.browser_alt) end,
		{group = group_name.default, description= "Launch Alternative Browser"}
	),
	awful.key({M, S}, "e",
		function() awful.spawn(apps.explorer_alt) end,
		{group = group_name.default, description= "Launch Alternative File manager"}
	),
	awful.key({M, S}, "v",
		function() awful.spawn(apps.editor_cmd) end,
		{group = group_name.default, description= "Launch cli text editor"}
	),
	awful.key({M}, "Return",
		function() awful.spawn(apps.terminal) end,
		{group = group_name.default, description= "Launch Terminal"}
	),
	awful.key({C, A}, "t",
		function() awful.spawn(apps.terminal_alt) end,
		{group = group_name.default, description= "Launch Alternative Terminal"}
	),

	-- Emacs
	-- TODO: emacs + emacs_config + to-do.org + awesome config
	awful.key({M, A}, "e",
		function() awful.spawn(apps.emacs) end,
		{group = group_name.rest, description = "Open emacs"}
	),

	-- Launcher [ Rofi ]
	awful.key({A}, "space",
		function() awful.spawn("rofi -show drun") end,
		{group = group_name.rest, description= "rofi -show drun"}
	),
	awful.key({A}, "x",
		function() awful.spawn("rofi -show run") end,
		{group = group_name.rest, description= "Rofi - Run script"}
	),
	awful.key({M}, "w",
		function() awful.spawn("rofi -show window") end,
		{group = group_name.rest, description= "Rofi - Windows list"}
	),

	-- Color picker
	awful.key({A}, "g",
		function() awful.spawn("colorpick copy") end,
		{group = group_name.rest, description= "Pick a color to clipboard"}
	),
	awful.key({A}, "j",
		function() awful.spawn("colorpick preview") end,
		{group = group_name.rest, description= "Pick a color with preview"}
	),

	-- Screenshots
	awful.key({}, "Print",
		function() awful.spawn(scripts_path .. "screenshot selective") end,
		{group = group_name.screenshot, description= "Partial screenshot to clipboard"}
	),
	awful.key({A}, "Print",
		function() awful.spawn(scripts_path .. "screenshot partial") end,
		{group = group_name.screenshot, description= "Save Partial screenshot"}
	),
	awful.key({M}, "Print",
		function() awful.spawn(scripts_path .. "screenshot full") end,
		{group = group_name.screenshot, description= "Take Full screenshot"}
	),
	awful.key({M, S}, "Print",
		function() awful.spawn(scripts_path .. "screenshot pin") end,
		{group = group_name.screenshot, description= "Open area with feh"}
	),
	-- awful.key({M}, "Print",
	-- 	function() awful.spawn(scripts_path .. "screenshot shadow") end,
	-- 	{group = group_name.screenshot, description= "Save a selected area with shadow effect"}
	-- ),
	-- awful.key({M}, "Print",
	-- 	function() awful.spawn("maim | gimp ..") end,
	-- 	{group = group_name.screenshot, description= "Open screenshot with image editor"}
	-- ),

	-- Record [ TODO ]
	-- ..

	-- Clipboard [ Greenclip]
	awful.key({A}, "m",
		function() awful.spawn("rofi -modi 'clipboard:greenclip print' -show clipboard -run-command '{cmd}'") end,
		{group = group_name.rest, description= "Launch greenclip clipboard manager"}
	)
}
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap

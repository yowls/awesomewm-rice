local awful = require('awful')

local group_name = "Screens"


-- MULTI MONITORS/SCREENS
awful.keyboard.append_global_keybindings {
	awful.key({M}, ".",
		function () awful.screen.focus_relative( 1) end,
		{group = group_name, description = "focus the next screen"}
	),
	awful.key({M}, ",",
		function () awful.screen.focus_relative(-1) end,
		{group = group_name, description = "focus the previous screen"}
	)
}

client.connect_signal("request::default_keybindings", function()
	awful.keyboard.append_client_keybindings {
		awful.key({M, S}, ".",
			function (c) c:move_to_screen(c.screen.index+1) end,
			{group = group_name, description = "move to next screen"}
		),
		awful.key({M, S}, ",",
			function (c) c:move_to_screen(c.screen.index-1) end,
			{group = group_name, description = "move to previous screen"}
		)
	}
end)
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap

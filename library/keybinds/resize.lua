local awful = require('awful')

local group_name = "Resize clients"


-- RESIZE CLIENTS
awful.keyboard.append_global_keybindings {
	awful.key({M}, "l",
		function () awful.tag.incmwfact( 0.05) end,
		{group = group_name, description = "increase master width factor"}
	),
	awful.key({M}, "h",
		function () awful.tag.incmwfact(-0.05) end,
		{group = group_name, description = "decrease master width factor"}
	)
}
-- vim: tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab nowrap

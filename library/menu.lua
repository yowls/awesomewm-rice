local awful = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup")
local beautiful = require("beautiful")

local settings = require("settings.default_apps")

local menu = {}		-- Store awesome desktop menu


--	SUB-MENUS
-- ===================
-- Awesome menu
menu.awesome = {
	{"Hotkeys",		function() hotkeys_popup.show_help(nil, awful.screen.focused()) end},
	{"Manual",		string.format("%s -e man awesome", settings.terminal)},
	{"Edit config",	string.format("%s %s", settings.editor_cmd, awesome.conffile)},
	{"Restart",		awesome.restart}
}

-- Exit session
-- TODO: add lock and shutdown
menu.exit = {
	{"Log out",		function() awesome.quit() end},
	-- {"Lock session",		function() awesome.quit() end},
	-- {"Shutdown",		function() awesome.quit() end},
}


--	ROOT MENU
-- ===================
menu.main = awful.menu({
	items = {
		{"Terminal", settings.terminal},
		{"File Manager", settings.explorer},
		{"Browser", settings.browser},
		{"Chat", settings.communication},
		{"Awesome", menu.awesome, beautiful.awesome_icon},
		{"Exit", menu.exit}
	}
})


return menu

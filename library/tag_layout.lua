---
-- SET TAGS SETTINGS AND LAYOUTS
-- based on user settings (settings/global.lua)
---

local awful = require('awful')

local helpers	= require("library.helpers.tag_layout")
local settings	= require("settings.global")


-- SET LAYOUTS
tag.connect_signal("request::default_layouts", function()
	-- Reset layouts
	-- awful.layout.layouts = {}

	-- Append layouts
	awful.layout.append_default_layouts(
		helpers.create_layouts(settings.layouts_list)
	)
end)


-- SET TAGS
screen.connect_signal("request::desktop_decoration", function(s)
	local tls = settings.tag_label_screens[s.index]

	-- Create tags based on settings
	for k in pairs(tls) do
		if not tls then tls = settings.tag_label_screens[s.index-1] end -- fallback
		awful.tag.add(tls[k], {
			icon			= helpers.get_tag_icon(s.index, k),
			layout			= awful.layout.suit[ settings.tag_layout_screens[s.index][k] ],
			master_fill_policy	= "expand",
			master_count 		= 1,
			column_count 		= 1,
			gap			= settings.window_margin,
			gap_single_client	= true,
			screen			= s.index
		})
	end
end)
-- vim: tabstop=8:shiftwidth=8:softtabstop=0:noexpandtab:nowrap

local ruled = require('ruled')
local labels = require("settings.global").tag_label_screens


---------------------------
-- MATCH WINDOW IN  TAGS --
---------------------------

-- Load in screen 2 if 2nd monitor is connected or in 1 if is not
-- very useful for notebooks that only sometimes connect an extra screen
local dynamic_screen2 = (#screen == 2) and 2 or 1

-- Set applications in determinated tag of screen
-- index of tables (<screen>, <tags>) are applied in numerical order and not by name
-- example:
--  ...
-- 	[<screen>] = {
-- 		[<tag>] ={
-- 			<wm_class>
-- 		}
-- 	}
--  ...

local giga_match = {
	['1'] = {
		['4'] = {
			-- "TelegramDesktop",
			-- "pcmanfm-qt",
			"Nemo",
			"Xreader",
			"deemix-gui"
		},
		['5'] = {
			"discord",
			"zoom",
			"Microsoft Teams - Preview"
		},
		['6'] = {
			"Thunderbird"
		},
		['8'] = {
			"Spotify"
		},
		['9'] = {
			"KeePassXC",
			"Bitwarden"
		}
	},

	[dynamic_screen2] = {
		['1'] = {
			"kitty",
			"qterminal",
			"konsole"
		},
		['2'] = {
			"firefox",
			"Chromium-browser",
			"Vivaldi-stable"
		},
		['3'] = {
			"VSCodium",
			"Atom",
			"Emacs"
		},
		['6'] = {
			"OBS",
			"Kamoso"
		}
	},
}

-- Append rule if monitor 2 is connected
if #screen >= 2 then
	table.insert(giga_match, {
	['2'] = {
		['5'] = {
			"Figma",
			"Inkscape",
			"Gimp-2.10",
			"Libreoffice",
			"WPS"
		}
	}
	})
end


-- APPEND WINDOW MATCHING IN TAG RULES
-- TODO: validate if screen and tag exist
for sc,tgs in pairs(giga_match) do
	for tg, classes in pairs(tgs) do
		local nscreen	= tonumber(sc)
		local ntag	= tonumber(tg)
		ruled.client.append_rule {
			rule_any = { class=classes },
			properties = {
				screen	= screen[nscreen],
				tag	= labels[nscreen][ntag],
				switch_to_tags	= true
			}
		}
	end
end

-- vim: tabstop=8:shiftwidth=8:softtabstop=0:noexpandtab:nowrap

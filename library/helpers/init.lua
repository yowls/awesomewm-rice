-- Return the reference for others helpers
return {
	debug		= require('library.helpers.debug'),
	keybinds	= require('library.helpers.keybinds'),
	tag_layout	= require('library.helpers.tag_layout')
}

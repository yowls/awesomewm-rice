---
-- SET THE WALLPAPER
-- based on user options (settings/wallpaper)
---

local awful		= require('awful')
local wibox		= require('wibox')
local beautiful	= require('beautiful')
local gfs		= require('gears.filesystem')
local debug		= require('library.helpers.debug')

local settings	= require('settings.wallpaper')
local paths		= require('settings.paths')


-- Get all files from a directory
local function get_all_files(path)
	local cmd = "find " .. path
	local output = io.popen(cmd):lines()
	local content = {}
	for line in output do
		table.insert(content, line)
	end
	return content
end


-- Check file is in valid format
local function available_format(file)
	local formats = {".png", ".jpeg", ".jpg"}
	for _,format in pairs(formats) do
		if string.find(file, format) then
			return true
		end
	end
	return false
end


-- Random search of a file in a directory
local function random_wall(img_dir)
	local i = 1
	local files = get_all_files(img_dir)

	while true do
		local random_file = files[math.random(#files)]
		if available_format(random_file) then
			return random_file
		end
		if i > 9 then		-- try limit
			debug.notify("Cannot find a correct wallpaper", "fix the wallpaper path in settings")
			return beautiful.wallpaper
		end
		i = i+1
	end
end


-- Fallback wallpaper
local function fallback_wallpaper()
	debug.notify("Cannot find a correct wallpaper", "fix the wallpaper path in settings")
	return random_wall(paths.awesome_themes .. "wallpapers")
end


screen.connect_signal("request::wallpaper", function(s)

	if settings.method == "image" then
		-- OPTION 1: Set a static image as wallpaper

		local img
		if settings.randomize then
			local wall = settings.image_dir[s.index] or settings.image_dir[1]
			img = random_wall( wall )

		else
			local wall = settings.image_wall[s.index] or settings.image_wall[1]
			if gfs.file_readable( wall ) then
				img = wall
			else
				img = fallback_wallpaper()
			end
		end

		awful.wallpaper {
			screen = s,
			widget = {
				image	= img,
				valign	= "center",
				halign	= "center",
				resize	= true,
				widget	= wibox.widget.imagebox
			}
		}

	-- =================================================

	elseif settings.method == "animated" then
		-- OPTION 2: Manage animated wallpaper with xwinwrap

		print("not ready")
		-- if settings.randomize then
		-- else
		-- end

	-- =================================================

	else
		-- OPTION 3: Set solid color as wallpaper

		local color
		if settings.randomize then
			-- load colors from color scheme
			local colors = {}
			for _, i in pairs(beautiful.cs) do
				colors[#colors+1] = i
			end

			color = colors[math.random(#colors)]
		else
			color = settings.color_wall
		end
		awful.wallpaper {
			screen	= s,
			bg		= color
		}
	end
end)

---
-- Check if is the first load
-- to show some useful information
---

local awful = require('awful')
local debug = require('library.helpers.debug')
local paths = require('settings.paths')


-- Run the first terminal found and enter in awesome config
local terminal_list = {"kitty", "urxvt", "konsole"}
local test = "command -v " .. terminal_list[1]
awful.spawn.easy_async_with_shell(test, function(stdout)
	if stdout then
		-- try to launch terminal
		awful.spawn(choice)
	else
		-- otherwise search for another terminal
	end
end)


-- Send some messages
debug.notify('READ ME', 'All logs will be in: ' .. paths.awesome_cache, 20, 'critical')


-- Check if file exist, otherwise touch it
local location_file = paths.awesome_cache .. "loaded"

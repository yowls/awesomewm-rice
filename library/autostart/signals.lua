---
-- AWESOME SIGNALS
---

local awesome, client, screen = awesome, client, screen
local awful = require("awful")
local beautiful = require("beautiful")

-- =================================================

-- {{{ FOCUS

-- Prevent clients from being unreachable after screen count changes.
client.connect_signal("manage", function (c)
	if awesome.startup
		and not c.size_hints.user_position
		and not c.size_hints.program_position then
			awful.placement.no_offscreen(c)
			-- awful.placement.no_overlap(c)
	end
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:activate { context = "mouse_enter", raise = false }
end)

if beautiful.border_width > 0 then
	client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
	client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
end

-- NOTE: This fix the none selected tag at start
-- TODO: check when restarting awesome
if awesome.startup then
	for s in screen do
		screen[s.index].tags[2].selected = true
	end
end

-- }}}

-- =================================================

--  {{{ LAYOUT

-- TODO: Remove border for maximized windows
client.connect_signal("property::maximized", function(c)
	if c.maximized then
		c.border_width = 0
	else
		-- restore width ??
		c.border_width = beautiful.border_width
	end
end)

-- TODO: Remove titlebar when not floating
-- client.connect_signal('manage', function(c)
-- end)

-- TODO: Add more border width for floating windows
-- client.connect_signal('property::floating', function(c)
-- end)

-- }}}

-- =================================================

-- {{{ STATUS BAR

-- Hide statusbar when going to full screen mode
local function update_bars_visibility()
	for s in screen do
		if s.selected_tag then
			local fullscreen = s.selected_tag.fullscreen_mode
			if s.main_bar then s.main_bar.visible = not fullscreen end
			if s.compost_bar then s.compost_bar.visible = not fullscreen end
			if s.external then s.external.visible = not fullscreen end
		end
	end
end

tag.connect_signal('property::selected', function(_)
	update_bars_visibility()
end)

client.connect_signal('property::fullscreen', function(c)
	if c.first_tag then
		c.first_tag.fullscreen_mode = c.fullscreen
	end
	update_bars_visibility()
end)

client.connect_signal('unmanage', function(c)
	if c.fullscreen then
		c.screen.selected_tag.fullscreen_mode = false
		update_bars_visibility()
	end
end)

-- }}}

-- =================================================

-- TODO: GARBAGE COLLECTOR

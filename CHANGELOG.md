# 📖 CHANGELOG
Log of major code changes

<br>
<br>

## [0.2.X] 2022-01-XX (WIP)
### ✨ Added
+ 'Extras' submodule

### 💥 Changes
+ Updating from gears.wallpaper to awful.wallpaper
+ Removed old config of naughty
+ Log files now have the .log extension


<br>
<br>


## [0.2.3] 2021-12-24
### 📜 The promised day
### ✨ Added
+ Different font selection for titlebar, statusbar, notification and global

### 🐛 Fixed
+ Volume and Brightness scripts now dont stack notifications
+ Hiding statusbar in fullscreen
+ Statusbar widget icon color
+ Desktop menu

### 💥 Changes
+ Refactoring themes/init (fragmenting into multiple files)
+ Refactoring tag rules
+ Improving the algorithm a bit


<br>
<br>


## [0.2.2] 2021-12-10
### ✨ Added
+ Light and Dark mode for every color scheme
+ New style repository assets v2.0
+ Option to delay startup applications

### 🔥 Removed
+ Color\_apps feature moved to awesomewm-extras repository

### 💥 Changes
+ Refactoring rules, keybinds (fragmenting into multiple files)
+ Moving tag and layout config to library/tag\_layout.lua
+ Renaming "color\_scheme" folder to "colors"


<br>
<br>


## [0.2.1] 2021-12-01
### 📜 Dock update
### ✨ Added
+ PKGBUILD for easy installation for Arch users
+ A setting file for configure Dock items
+ Dock at the bottom of the screen

### 🗑️ Deprecated
+ Colorize apps feature will be removed


<br>
<br>


## [0.2.0] 2021-11-17
### ✨ Added
+ Awesomectl, a lua script to:
  - Change, Get or List theme of a element
  - Read lua file using awesome-client
+ Setting file (paths) for store directories path
+ Icons to some widgets
+ Battery, Ram and a Volume widget

### 🐛 Fixed
+ Setting random wallpaper with image and color now works
+ Prompt widget

### 💥 Changes
+ Upgrades to 'Solo' status bar theme


<br>
<br>


## [0.1.1] 2021-10-30
### ✨ Added
+ Pywal-like in lua for some appplications (color\_apps)
+ New wallpapers in themes folder (6)
+ 'Min' icon theme
+ Improving the statusbar (new widget, new layouts)

### 🐛 Fixed
+ Notification timeout


<br>
<br>


## [0.1.0] 2021-09-26
### ✨ Added
+ User configs in the settings folder
+ Preset config for some programs inside the settings folder (picom,redshift,flashfocus)
+ Creating a 'Solo' theme for titlebar, statusbar, notification
+ Creating color schemes themes (5)
+ Script for launch applications at the awesome start
+ Lua script for changing the volume, brightness and taking screenshots
+ Xephyr script for launch Awesome in a nested session

#!/usr/bin/env lua

-- lua script for brightness management
-- require: light
-- https://github.com/haikarainen/light

-- given argument
local input = arg[1]


-- Help message
local function help()
	print("Description: lua script for brightness management")
	print("usage: brightness [ up | down | help ]\n")
	print("          -> up           | increase screen brightness by 5%")
	print("          -> down         | decrease screen brightness by 5%")
	print("          -> help         | print this message")
end


-- notify with awesome naughty lib
local function notify_naughty(msg1, msg2)
	local subtext = msg2 or ""

	local code = string.format([[
local naughty	= require("naughty")
local beautiful	= require("beautiful")
local recolor	= require("gears.color").recolor_image
local icon		= beautiful.icons.system.brightness
local brightness_icon = recolor(icon, beautiful.bg_focus)

if nb and not nb.is_expired then
	nb.message	= "%s \n %s"
else
	nb = naughty.notification {
		app_name	= "Brightness script",
		timeout		= 2,
		icon		= brightness_icon,
		title		= "<b>Brightness</b>",
		message		= "%s \n %s"
	}
end
	]], msg1, subtext, msg1, subtext)

	local cmd = string.format("awesome-client '%s'", code)
	os.execute(cmd)
end


-- Send notification function
local function notify_brightness()
	local cmd = io.popen("light -G")
	local cmd_out = cmd:read("*a")	-- get float
	cmd:close()
	local current = math.ceil(cmd_out)

	local bar_size = current // 10
	local percentage_bar = string.rep("━", bar_size) .. "┫"

	notify_naughty(percentage_bar, current)
end


-- Awesome emit signal
local function send_signal()
	local code = [[ awesome.emit_signal("widget::brightness") ]]

	local cmd = string.format("awesome-client '%s'", code)
	os.execute(cmd)
end


-- MAIN
if input == nil then
	-- if no arg given, then print current brightness
	notify_brightness()
	help()

else
	if input == "up" then
		if os.execute("light -A 5") then
			notify_brightness()
			send_signal()
		else
			notify_naughty("Light is not installed")
		end

	elseif input == "down" then
		if os.execute("light -U 5") then
			notify_brightness()
			send_signal()
		else
			notify_naughty("Light is not installed")
		end

	elseif input == "-h" or input == "--help" or input == "help" then
		help()
		os.exit(0)

	else
		print('\27[31m[Error: invalid option]\27[0m')
		help()
		os.exit(1)
	end
end
